
------------------------------------------ UF3-2-P6: Introducció als cursors “empresa” ------------------------------------------

*** Cursors “for update”

7. Fer un procediment que rebi per paràmetre el nom d'un departament i el percentatge de pujada de salari. El procediment incrementarà el salari el percentatge que li indiquem als  empleats que pertanyen al departament rebut i informarà de a quants empleats se'ls ha modificat el salari. El percentatge es rebrà en tant per 1 (és a dir, el 10% seria 0,1 etc).

8. Modifiqueu el procediment anterior per tal que ens mostri el número d’empleats modificats. Cal actualitzar cada salari individualment i fer servir el ROWID.

9. Crear un procediment que rebi el número d’empleat i la quantitat que s’incrementa el salari de l’empleat corresponent. Feu servir dos excepcions: una definida per l’usuari (salari_nul) i una altra predeterminada (NO_DATA_FOUND)

10. Procediment que permeti pujar el sou de tots els empleats que guanyin menys que el salari mig del seu ofici. La pujada serà del 50% de la diferència entre el salari de l’empleat i la mitja del seu ofici. Cal terminar la transacció i gestionar els possibles errors.


------------------------------------------ UF3-2-P10: Triggers sobre bancs ------------------------------------------

1. Crea un trigger que es digui “verif_crea_compte” que serveixi per no permetre crear un nou compte amb data d'alta posterior a la data actual ni amb “saldo debe” positiu ni amb “saldo haber” negatiu o zero (és a  dir, per crear un compte s'han de tenir diners).
Es podria haver implementat alguna d'aquestes condicions com a constraint de la taula
“cuentas”?

2. Implementa allò que sigui necessari per garantir que el tipus de moviment de la taula “movimientos” només pugui ser “I” (ingrès) o “R” (reintegrament) , ambdues en majúscules.
Has de plantejar-te què es podria fer en el cas de què en les dades actuals que ja hi són a la taula hi hagués algun registre que ja no compleixi aquesta condició.
També has de pensar si es permetrà que l'usuari fiqui aquestes lletres en minúscules o no.

3. Implementa el trigguer o triggers necessaris per garantir que:
.- Si es dóna d'alta un nou moviment la data no pugui ser anterior a la d'avui.
.- Si es dòna d'alta un nou moviment o es modifica el tipus de moviment, o l'import del moviment o s'elimina un moviment el valor del “saldo debe” i el valor del “saldo haber” siguin alterats en conseqüència.
.- No es pugui canviar un moviment d'un compte a un altre.
El valor del “saldo debe” del compte ha de ser sempre la suma de tots els reintegraments que s'han fet d'aquest compte i el valor del “saldo haber” ha de ser la suma de tots els ingressos.
No s'han de recalcular els saldos en cada operació a partir de tots els moviments del compte, només s'han de modificar conseqüentment a l'operació feta sobre els moviments assumint que el valor de partida dels saldos eren els correctes.
Tingues en compte tots els atributs que formen la clau primària del compte!

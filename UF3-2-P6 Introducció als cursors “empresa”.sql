-- 1. Bloc anònim que visualitzi el nom i la localitat de tots els departaments.

DECLARE
  CURSOR c_depart IS SELECT dnom, loc FROM dept;
  v_nom VARCHAR2(14);
  v_localitat VARCHAR2(14);
BEGIN
  OPEN c_depart;
  FETCH c_depart INTO v_nom, v_localitat;
  IF c_depart%NOTFOUND THEN
    dbms_output.put_line('No s''ha trobat cap registre');
  ELSE
    WHILE c_depart%FOUND LOOP
      dbms_output.put_line('El nom és: '||v_nom||'. La localitat és: '||v_localitat);
      FETCH c_depart INTO v_nom, v_localitat;
    END LOOP;
  END IF;
  CLOSE c_depart;
END;

-- 2. Bloc anònim que mostri els cognoms dels empleats que pertanyen al departament de “VENDES”.
-- Cal numerar cada línia seqüencialment. Pensa què respondria si: La taula emp o dept estiguessin buides? NO_DATA_FOUND
-- Si no hi hagués departament "vendes"? NO_DATA_FOUND
-- Si no hi hagués cap empleat al departament "vendes"? NOTFOUND

DECLARE
  CURSOR c_cognoms IS SELECT cognom FROM emp JOIN dept USING(dept_no) WHERE UPPER(dnom) LIKE UPPER('VENDES');
  v_cognom emp.cognom%type;
  v_vendes dept.dept_no%type;
BEGIN
  OPEN c_cognoms;
  FETCH c_cognoms INTO v_cognom;
  IF c_cognoms%NOTFOUND THEN
    dbms_output.put_line('No s''ha trobat cap registre');
  ELSE
    WHILE c_cognoms%FOUND LOOP
      dbms_output.put_line('El cognom és: '||v_cognom);
      FETCH c_cognoms INTO v_cognom;
    END LOOP;
  END IF;
  CLOSE c_cognoms;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No he trobat informació');
  WHEN OTHERS THEN
    dbms_output.put_line('Missatge error: '||SQLERRM||' Codi error: '||SQLCODE);
END;

-- 3. Crea un procediment que visualitzi per pantalla els cognoms dels empleats del departament que es rep com a argument
-- (es rep el codi del departament).
-- Fes dues versions, una fent servir un “for de cursor” i altra sense fer-ho servir.
-- Totes dues versions han d'informar en el cas de què no es mostri cap empleat
-- (bé perquè el departament demanat no existeixi o perquè no tingui empleats).

CREATE OR REPLACE PROCEDURE VEURES_COGNOMS
(
  v_dept_no dept.dept_no%type
) AS
  CURSOR c_cognoms IS SELECT cognom FROM emp WHERE dept_no = v_dept_no;
  v_cognom emp.cognom%type;
BEGIN
  OPEN c_cognoms;
  FETCH c_cognoms INTO v_cognom;
  IF c_cognoms%NOTFOUND THEN
    dbms_output.put_line('No s''ha trobat cap registre');
  ELSE
    WHILE c_cognoms%FOUND LOOP
      dbms_output.put_line('El cognom és: '||v_cognom);
      FETCH c_cognoms INTO v_cognom;
    END LOOP;
  END IF;
  CLOSE c_cognoms;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No he trobat informació');
  WHEN OTHERS THEN
    dbms_output.put_line('Missatge error: '||SQLERRM||' Codi error: '||SQLCODE);
END VEURES_COGNOMS;

-- Versio for cursor
CREATE OR REPLACE PROCEDURE VEURES_COGNOMS
(
  v_dept_no IN dept.dept_no%type
) AS
  CURSOR c_cognoms IS SELECT cognom FROM emp WHERE dept_no = v_dept_no;
  v_count NUMBER;
BEGIN
  SELECT count(*) INTO v_count FROM emp WHERE dept_no = v_dept_no;
  IF v_count < 0 THEN
  dbms_output.put_line('No s''ha trobat cap registre');
  END IF;
  FOR v_cognom IN c_cognoms LOOP
    dbms_output.put_line('El cognom és: '||v_cognom.cognom);
  END LOOP;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No he trobat informació');
  WHEN OTHERS THEN
    dbms_output.put_line('Missatge error: '||SQLERRM||' Codi error: '||SQLCODE);
END VEURES_COGNOMS;

-- 4. Crea una funció que serveixi per calcular quants empleats del departament que passem per paràmetre cobren comissió (vol dir comissió major que 0).
-- Passem per paràmetre el codi del departament.
-- Fes-ho sense fer servir la funció d'agregació COUNT().
-- Si el departament no existeix o no té cap empleat la funció ha de tornar el valor NULL.
-- Si el departament sí té empleats però no n'hi ha cap que cobri comissió la funció ha de tornar un 0.
-- S'han de fer dues versions: la primera sense fer servir el “for de cursor” i la segona fent-ho servir.

CREATE OR REPLACE FUNCTION TEST_COMISSIO
(
  v_dept_no dept.dept_no%type
) RETURN NUMBER AS
  CURSOR c_comissio IS SELECT comissio FROM emp WHERE dept_no = v_dept_no AND comissio > 0;
  v_contador NUMBER(2):= 0;
  v_comissio emp.comissio%type;
BEGIN
  OPEN c_comissio;
  FETCH c_comissio INTO v_comissio;
  IF c_comissio%NOTFOUND THEN
      RETURN 0;
  ELSE
    WHILE c_comissio%FOUND LOOP
      FETCH c_comissio INTO v_comissio;
      v_contador:= v_contador + 1;
    END LOOP;
  END IF;
  CLOSE c_comissio;
  RETURN v_contador;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN NULL;
END TEST_COMISSIO;

-- Versio for
CREATE OR REPLACE FUNCTION TEST_COMISSIO
(
  v_dept_no dept.dept_no%type
) RETURN NUMBER AS
  CURSOR c_comissio IS SELECT comissio FROM emp WHERE dept_no = v_dept_no AND comissio > 0;
  v_contador NUMBER(2):= 0;
BEGIN
  FOR v_comissio IN c_comissio LOOP
    v_contador:= v_contador + 1;
  END LOOP;
  RETURN v_contador;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN NULL;
END TEST_COMISSIO;

-- 5. Fes un procediment que fent servir la funció de l'exercici 4 mostri per pantalla el codi
-- i nom dels departaments en què menys de la meitat dels seu empleats cobren comissió.
-- Si el departament no té cap empleat s'ha de mostrar un missatge informatiu.
-- (També amb les dues versions, sense i amb “for de cursor”).

CREATE OR REPLACE PROCEDURE exercici5
(
  v_dept_no dept.dept_no%type
) AS
  v_num_emp NUMBER(2);
  v_test_comisio NUMBER(2);
  v_nom_dept dept.dnom%type;
BEGIN
  SELECT count(dept_no) INTO v_num_emp FROM emp WHERE dept_no = v_dept_no;
  SELECT dnom INTO v_nom_dept FROM dept WHERE dept_no = v_dept_no;
  v_test_comisio := TEST_COMISSIO(v_dept_no);
  CASE v_test_comisio
    WHEN 0 THEN dbms_output.put_line('Cap cobra comissio');
    WHEN -1 THEN dbms_output.put_line('El departament no existeix o no hi ha cap empleat');
    ELSE
      IF(v_test_comisio < v_num_emp) THEN
      dbms_output.put_line('Nom departament: '||v_nom_dept ||' Codi departament: '|| v_dept_no);
      END IF;
  END CASE;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No he trobat informació');
  WHEN OTHERS THEN
    dbms_output.put_line('Missatge error: '||SQLERRM||' Codi error: '||SQLCODE);
END exercici5;

-- 6. Programa que mostri per cada departament una línia amb el número d’empleats i suma dels salaris del departament a continuació
--    la llista dels cognoms i salaris dels empleats d'aquest  departament. Al final del llistat: número total d’empleats de l'empresa
--    i suma de tos els salaris.

CREATE OR REPLACE PROCEDURE EXERCICI6
AS
  CURSOR c_dept IS SELECT * FROM dept;
  CURSOR c_emp IS SELECT * FROM emp;
  v_dept dept%rowtype;
  v_emp emp%rowtype;
  v_emp_count NUMBER(2);
  v_total_emp NUMBER(2);
  v_emp_sum_salari emp.salari%type;
  v_total_salari emp.salari%type;
BEGIN
  OPEN c_dept;
  FETCH c_dept INTO v_dept;
  IF c_dept%NOTFOUND THEN
    dbms_output.put_line('No s''ha trobat cap registre');
  ELSE
    dbms_output.put_line('Informació dels departaments');
    WHILE c_dept%FOUND LOOP
      SELECT COUNT(emp_no) INTO v_emp_count FROM emp WHERE dept_no = v_dept.dept_no;
      SELECT NVL(SUM(salari),0) INTO v_emp_sum_salari FROM emp WHERE dept_no = v_dept.dept_no;
      dbms_output.put_line('Codi departament: '||v_dept.dept_no||' Número d''empleats: '||v_emp_count||' Suma salaris: '||v_emp_sum_salari||' €');
      FETCH c_dept INTO v_dept;
    END LOOP;
  END IF;
  CLOSE c_dept;
  dbms_output.put_line(' ');
  OPEN c_emp;
  FETCH c_emp INTO v_emp;
  IF c_emp%NOTFOUND THEN
    dbms_output.put_line('No s''ha trobat cap registre');
  ELSE
      dbms_output.put_line('Informació dels empleats');
    WHILE c_emp%FOUND LOOP
      dbms_output.put_line('Cognom: '||v_emp.cognom||' Salari: '||v_emp.salari||' €');
    FETCH c_emp INTO v_emp;
  END LOOP;
  END IF;
  CLOSE c_emp;
  SELECT COUNT(emp_no) INTO v_total_emp FROM emp;
  SELECT SUM(salari) INTO v_total_salari FROM emp;
  dbms_output.put_line(' ');
  dbms_output.put_line('Informació empresa');
  dbms_output.put_line('Empleats totals: '||v_total_emp||' Suma total salaris: '||v_total_salari||' €');
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No he trobat informació');
  WHEN OTHERS THEN
    dbms_output.put_line('Missatge error: '||SQLERRM||' Codi error: '||SQLCODE);
END EXERCICI6;

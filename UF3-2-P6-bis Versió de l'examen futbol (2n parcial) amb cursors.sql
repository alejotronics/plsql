-- 1) Crea un procediment que es digui «proc_puntua» que serveixi per calcular i gravar a la taula equips els punts aconseguits per cada equip en aquesta competició.
-- El procediment no rebrà cal argument, calcularà i gravarà els punts de cada equip tenint en compte que per cada partit guanyat  a casa l’equip aconsegueix 2 punts,
-- per cada partit empatat a casa o fora de casa l’equip aconsegueix 1 punt i per cada partit guanyat fora de casa l’equip aconsegueix 3 punts.
-- Cal mostrar un missatge informatiu si no hi ha cap equip a la taula o es produeix qualsevol altre error inesperat */
CREATE OR REPLACE PROCEDURE proc_puntua
AS
  v_punts equips.punts%type:=0;
  CURSOR c_punts IS SELECT nom_equip FROM equips FOR UPDATE;
BEGIN
  FOR v_equips IN c_punts LOOP
    v_punts := func_puntua_bo(v_equips.nom_equip);
    CASE v_punts
      WHEN -1 THEN dbms_output.put_line('No hi ha cap equip amb aquest nom');
      WHEN -2 THEN dbms_output.put_line('Hi ha més d''un empleat amb aquest codi , no pot ser');
      WHEN -3 THEN dbms_output.put_line ('Error en l''aplicació. Error codi: ' || SQLCODE|| SQLERRM);
      ELSE UPDATE equips SET punts = v_punts WHERE CURRENT OF c_punts;
    END CASE;
  END LOOP;
END proc_puntua;

CREATE OR REPLACE FUNCTION func_puntua_bo
(
  v_nom equips.nom_equip%TYPE
) RETURN NUMBER AS
    v_punts NUMBER(3):=0;
    v_id_equip equips.id_equip%TYPE;
    v_guanya_local NUMBER(2):=0;
    v_guanya_visitant NUMBER(2):=0;
    v_empat NUMBER(2):=0;
BEGIN
  SELECT id_equip INTO v_id_equip FROM equips WHERE UPPER(nom_equip) LIKE UPPER(v_nom);
  -- partits guanyats a casa
  SELECT COUNT(*) INTO v_guanya_local FROM equips JOIN partits ON id_equip = id_equip_local WHERE id_equip = v_id_equip AND gols_local > gols_visitant;
  v_punts:= v_guanya_local *2;
  -- partits empatats a casa
  SELECT COUNT(*) INTO v_empat FROM equips JOIN partits ON id_equip = id_equip_local WHERE id_equip = v_id_equip AND gols_local = gols_visitant;
  v_punts := v_punts + v_empat;
  --partits empatats fora
  SELECT COUNT(*) INTO v_empat FROM equips JOIN partits ON id_equip = id_equip_visitant WHERE id_equip = v_id_equip AND gols_local = gols_visitant;
  v_punts := v_punts + v_empat;
  -- partits guanyat fora
  SELECT COUNT(*) INTO v_guanya_visitant FROM equips JOIN partits ON id_equip = id_equip_visitant WHERE id_equip = v_id_equip AND gols_visitant > gols_local;
  v_punts := v_punts + v_guanya_visitant * 3;
  DBMS_OUTPUT.PUT_LINE('Punts calculats: '||v_punts);
  RETURN v_punts;
EXCEPTION
   WHEN NO_DATA_FOUND THEN
       RETURN -1;
   WHEN TOO_MANY_ROWS THEN
       RETURN -2;
   WHEN OTHERS THEN
       RETURN -3;
END func_puntua_bo;

-- 2/3) Crea un procediment que es digui «proc_prima_tots» que servirà per calcular la «prima» (gratificació econòmica)
-- que s’haurà de pagar a un jugador a final de temporada d’aquesta competició.
-- El procediment no rebrà cap argument, calcularà i gravarà la prima de cada jugador i mostrarà un missatge amb el
-- número de registres actualitzats.
-- Es farà servir una funció pel càlcul de la prima (la de l'exercici 2 de l'examen modifica per tal que el
-- argument que rep sigui el codi del jugador i que tingui en compte:
-- El jugador rebrà 5000 euros en concepte d’ajuda als desplaçaments si és d’un equip d’un país diferent al seu de procedència.
-- El jugador rebrà 1000 euros per cada minut que hagi jugat (en tota la temporada).
-- El jugador rebrà 2000 euros per cada partit que hagi guanyat el seu equip (hi hagi jugat ell o no).
-- Si a la taula no hi hagués cap jugador es mostraria un missatge informatiu.
-- Si es produeix qualsevol error inesperat també s'ha de mostrar un missatge*/

CREATE OR REPLACE PROCEDURE proc_prima_tots
AS
  v_prima jugadors.prima%type:=0;
  CURSOR c_prima IS SELECT id_jugador FROM jugadors FOR UPDATE;
BEGIN
  FOR v_jugadors IN c_prima LOOP
    v_prima := fun_prima_bo(v_jugadors.id_jugador);
    CASE v_prima
      WHEN -1 THEN dbms_output.put_line('No hi ha cap jugador amb aquest nom');
      WHEN -2 THEN dbms_output.put_line('Hi ha més d''un jugador amb aquest codi, no pot ser');
      WHEN -3 THEN dbms_output.put_line ('Error en l''aplicació. Error codi: ' || SQLCODE|| SQLERRM);
      ELSE UPDATE jugadors SET prima = v_prima WHERE CURRENT OF c_prima;
      dbms_output.put_line('S''han actualitzat la prima del jugador: '||v_jugadors.id_jugador||' prima: '||v_prima||' €');
    END CASE;
  END LOOP;
END proc_prima_tots;

CREATE OR REPLACE FUNCTION fun_prima_bo (v_id_jugador jugadors.id_jugador%TYPE) RETURN NUMBER
AS
  v_prima            NUMBER(10):=0;
  v_pais_equip       equips.id_pais%TYPE;
  v_pais_jugador     jugadors.id_pais%TYPE;
  v_id_equip         jugadors.id_equip%TYPE;
  v_guanya_local     NUMBER(2):=0;
  v_guanya_visitant  NUMBER(2):=0;
  v_minuts           NUMBER(4):=0;
BEGIN
  SELECT id_equip INTO v_id_equip FROM jugadors WHERE id_jugador LIKE v_id_jugador;
  SELECT jugadors.id_pais, equips.id_pais INTO v_pais_jugador,  v_pais_equip FROM jugadors JOIN equips USING(id_equip) WHERE jugadors.id_jugador LIKE v_id_jugador;
  IF v_pais_jugador NOT LIKE v_pais_equip THEN
    v_prima := 5000;
  END IF;
  -- calcular minuts que ha jugat
  SELECT NVL(SUM(minuts),0) INTO v_minuts FROM jugadors JOIN participacions USING(id_jugador) WHERE id_jugador LIKE v_id_jugador;
  v_prima := v_prima + v_minuts * 1000;
  -- comptar partits guanyats per l'equip a casa
  SELECT COUNT(*) INTO v_guanya_local FROM equips JOIN partits ON id_equip LIKE id_equip_local
  WHERE id_equip = v_id_equip AND gols_local > gols_visitant;
  -- comptar partits guanyats per l'equip com a visitant
  SELECT COUNT(*) INTO v_guanya_visitant FROM equips JOIN partits ON id_equip LIKE id_equip_visitant
  WHERE id_equip LIKE v_id_equip AND gols_visitant > gols_local;
  v_prima := v_prima + (v_guanya_local + v_guanya_visitant) * 2000;
  RETURN v_prima;
END fun_prima_bo;

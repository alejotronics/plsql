-- 1. Crea una funció “Nomina1” que rebi com a argument el codi d'un empleat i calculi l'import que ha de cobrar cada mes tenim en compte que:
 -- • El salari que figura a la taula “emp” és el sou brut anual.
 -- • Els nostres treballadors cobren 14 pagues iguals a l'any.
 -- • Cada mes els hi paguem també la comissió corresponent. La comissió que figura a la taula és la comissió anual (corresponent a 12 mesos).
 -- • L'import s'ha de calcular amb dues xifres decimals.

 CREATE OR REPLACE FUNCTION EX1_FUNCIONS_EMPRESA
(
  v_emp_no emp.emp_no%type
) RETURN NUMBER AS
  v_salari NUMBER(12,2);
  v_comissio emp.salari%type;
BEGIN
  SELECT salari, comissio INTO v_salari, v_comissio FROM emp WHERE emp_no = v_emp_no;
  v_salari := v_salari/14 + NVL(v_comissio,0)/12;
  RETURN v_salari;
END EX1_FUNCIONS_EMPRESA;

-- 2. Crea un procediment “ProcNomina1” que rebi com argument el cognom d'un empleat, calculi l'import de la seva nòmina mensual i
--    mostri per pantalla el codi de l'empleat, el seu cognom i aquest import. Per calcular l'import has de fer servir la funció que has creat a l'exercici anterior.

CREATE OR REPLACE FUNCTION NOMINA_2
(
  v_emp_no emp.emp_no%type,
  v_num_mes NUMBER
) RETURN NUMBER AS
  v_salari NUMBER(12,2);
  v_comissio emp.salari%type;
  v_emp_subordinats NUMBER;
BEGIN
  SELECT salari, NVL(comissio,0) INTO v_salari, v_comissio FROM emp WHERE emp_no = v_emp_no;
CASE v_num_mes
    WHEN 6 THEN v_salari := (v_salari/14)*2 + NVL(v_comissio,0)/12;
    WHEN 12 THEN
      SELECT COUNT(emp_no) INTO v_emp_subordinats FROM emp WHERE cap = v_emp_no;
      v_salari := (v_salari/14)*2 + v_comissio/12 + v_emp_subordinats*0.01*v_salari;
    WHEN 3 THEN
      SELECT COUNT(client_cod) INTO v_emp_subordinats FROM client WHERE repr_cod = v_emp_no;
      v_salari := (v_salari/14) + v_comissio/12 + v_emp_subordinats*0.02*v_salari;
    ELSE v_salari := v_salari/14 + v_comissio/12;
  END CASE;
RETURN v_salari;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN -1;
  WHEN OTHERS THEN
   RETURN SQLCODE;
END NOMINA_2;

-- CASOS DE PROVA -------------------------------------------------------------
-- • 46428.57    empleat 7839  mes 5
-- • 112357.14   empleat 7839  mes 12 (extra + comandament de 3 empleats)
-- • 18107.14    empleat 7499  mes 4
-- • 26427.14    empleat 7499  mes 3 (comp. productivitat 2 clients)
-- • 32964.29    empleat 7499  mes 6
-- • no existeix no data found
-- • 55714.29    empleat 7902  mes 12 sense empleats a càrrec
-- FI CASOS DE PROVA ----------------------------------------------------------

-- 4. Millora el procediment de l'exercici 2 de tal forma que es rebin com a arguments el cognom de l'empleat i el mes del qual s'ha de calcular la nòmina,
-- es cridi a la funció que has fet a l'exercici 3 per calcular-la i mostri per pantalla el codi de l'empleat, el seu cognom, l'import de la nòmina calculada i el mes al qual correspon.
-- Aquest procediment s'ha de dir “ProcNomina2”.

CREATE OR REPLACE PROCEDURE PROC_NOMINA_2
(
  v_cognom  emp.cognom%type,
  v_mes NUMBER
) AS
  v_emp_no emp.emp_no%type;
BEGIN
  SELECT emp_no INTO v_emp_no FROM emp WHERE UPPER(cognom) LIKE UPPER(v_cognom);
  DBMS_OUTPUT.PUT_LINE('Codi Empleat: ' || v_emp_no|| ' Cognom: ' || v_cognom || 'Mes: ' || v_mes || ' Salari Mensual: ' ||NOMINA_2(v_emp_no,v_mes));
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No s''ha trobat l''empleat');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('S''ha trobat més d''un empleat');
  WHEN OTHERS THEN
    dbms_output.put_line('Codi error: '||SQLCODE||' - '||SQLERRM);
END PROC_NOMINA_2;

-- 1. Crea un trigger que es digui “verif_crea_compte” que serveixi per no permetre
-- crear un nou compte amb data d'alta posterior a la data actual ni amb “saldo
-- debe” positiu ni amb “saldo haber” negatiu o zero (és a dir, per crear un
-- compte s'han de tenir diners). Es podria haver implementat alguna d'aquestes
-- condicions com a constraint de la taula “cuentas”? Si

CREATE OR REPLACE TRIGGER VERIF_CREA_COMPTE
  BEFORE INSERT ON CUENTAS
  FOR EACH ROW WHEN (new.fecha_alta > SYSDATE AND (new.saldo_debe >= 0 OR new.saldo_haber >= 0))
  DECLARE error EXCEPTION;
BEGIN
    RAISE error;
END;

-- 2. Implementa allò que sigui necessari per garantir que el tipus de moviment de
-- la taula “movimientos” només pugui ser “I” (ingrès) o “R” (reintegrament),
-- ambdues en majúscules. Has de plantejar-te què es podria fer en el cas de què en
-- les dades actuals que ja hi són a la taula hi hagués algun registre que ja no
-- compleixi aquesta condició. També has de pensar si es permetrà que l'usuari
-- fiqui aquestes lletres en minúscules o no.

-- Faria un procediment que recorres tots els registres i verifiques que són 'R'
-- o 'I' si no ho són que ho consulti a l'usuari i faci un update

-- Comprovar que el TIPO_MOV sigui 'R' o 'I' en els nous movimets o els updates.
CREATE OR REPLACE TRIGGER LIMITARMOVIMENTS
  BEFORE INSERT OR UPDATE OF TIPO_MOV ON MOVIMIENTOS
  FOR EACH ROW
DECLARE
tipo_mov_error EXCEPTION;
BEGIN
  IF :NEW.TIPO_MOV NOT LIKE 'I' OR :NEW.TIPO_MOV NOT LIKE 'R' THEN
    RAISE tipo_mov_error;
  END IF;
END;

-- 3. Implementa el trigguer o triggers necessaris per garantir que:
-- .- Si es dóna d'alta un nou moviment la data no pugui ser anterior a la d'avui.

CREATE OR REPLACE TRIGGER CHECK_FECHA
  BEFORE INSERT ON MOVIMIENTOS
  FOR EACH ROW
DECLARE fecha_incorrecta EXCEPTION;
BEGIN
  IF :NEW.FECHA_MOV > SYSDATE THEN
    RAISE fecha_incorrecta;
  END IF;
END CHECK_FECHA;

-- .- Si es dòna d'alta un nou moviment o es modifica el tipus de moviment, o
-- l'import del moviment o s'elimina un moviment el valor del “saldo debe” i el
-- valor del “saldo haber” siguin alterats en conseqüència.
-- .- No es pugui canviar un moviment d'un compte a un altre.
-- El valor del “saldo debe” del compte ha de ser sempre la suma de tots els
-- reintegraments que s'han fet d'aquest compte i el valor del “saldo haber” ha de
-- ser la suma de tots els ingressos.
-- No s'han de recalcular els saldos en cada operació a partir de tots els
-- moviments del compte, només s'han de modificar conseqüentment a l'operació feta
-- sobre els moviments assumint que el valor de partida dels saldos eren els
-- correctes.
-- Tingues en compte tots els atributs que formen la clau primària del compte!

CREATE OR REPLACE TRIGGER CHECK_MOVIMENTS
  BEFORE DELETE OR INSERT OR UPDATE OF NUM_CTA, TIPO_MOV ON MOVIMIENTOS
  FOR EACH ROW
DECLARE
  v_importe_total MOVIMIENTOS.IMPORTE%TYPE := 0;
BEGIN
  SELECT NVL(SUM(importe),0) INTO v_importe_total FROM movimientos WHERE num_cta = :new.num_cta AND tipo_mov = :new.tipo_mov;
  IF deleting THEN
    UPDATE cuentas SET saldo_haber = saldo_haber + v_importe_total WHERE num_cta = :old.num_cta;
    UPDATE cuentas SET saldo_debe = saldo_debe + v_importe_total WHERE num_cta = :old.num_cta;
  END IF;
  IF updating OR deleting THEN
    IF :new.tipo_mov LIKE 'I' THEN
      UPDATE cuentas SET saldo_haber = saldo_haber + v_importe_total WHERE num_cta = :new.num_cta;
    END IF;
    IF :new.tipo_mov LIKE 'R' THEN
      UPDATE cuentas SET saldo_debe = saldo_debe + v_importe_total WHERE num_cta = :new.num_cta;
    END IF;
  END IF;
END;

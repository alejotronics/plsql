-- exercici 1

create or replace PROCEDURE PROC_PUNTUA
(
  v_nom_equip equips.nom_equip%type
) AS
  v_id_equip equips.id_equip%type;
  v_punts NUMBER;
BEGIN
  SELECT id_equip INTO v_id_equip FROM equips WHERE UPPER(nom_equip) LIKE UPPER(v_nom_equip);
  SELECT COUNT(*)*2 INTO v_punts FROM partits WHERE gols_local > gols_visitant AND UPPER(id_equip_local) = UPPER(v_id_equip);
  SELECT v_punts + COUNT(*)*3 INTO v_punts FROM partits WHERE gols_local < gols_visitant AND UPPER(id_equip_visitant) = UPPER(v_id_equip);
  SELECT v_punts + COUNT(*) INTO v_punts FROM partits WHERE gols_local = gols_visitant AND UPPER(id_equip_local) = UPPER(v_id_equip);
  SELECT v_punts + COUNT(*) INTO v_punts FROM partits WHERE gols_local = gols_visitant AND UPPER(id_equip_visitant) = UPPER(v_id_equip);
  SYS.DBMS_OUTPUT.PUT_LINE(v_punts);
  IF v_punts = 0 THEN
    DBMS_OUTPUT.PUT_LINE('L''equip '||v_nom_equip || ' no ha jugat cap partit');
  ELSE
    DBMS_OUTPUT.PUT_LINE('L''equip '||v_nom_equip || ' ha obtingut un total de '||v_punts||' punts');
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('L''equip introduit no s''ha pogut trobar');
  WHEN OTHERS THEN
    dbms_output.put_line(SQLERRM||' - '||SQLCODE);
END PROC_PUNTUA;

-- exercici 2

create or replace FUNCTION FUN_PRIMA
(
  v_nom_jugador jugadors.nom_jugador%type
) RETURN NUMBER AS
    v_jugador jugadors%rowtype;
    v_id_pais equips.id_pais%type;
    v_minuts NUMBER(2,0);
    v_guanyat NUMBER;
BEGIN
  SELECT * INTO v_jugador FROM jugadors WHERE UPPER(nom_jugador) LIKE UPPER(v_nom_jugador);
  SELECT id_pais INTO v_id_pais FROM equips WHERE id_equip = v_jugador.id_equip;
  IF v_jugador.id_pais != v_id_pais THEN
    v_jugador.prima := 5000;
  ELSE
      v_jugador.prima := 0;
  END IF;
  SELECT SUM(minuts) INTO v_minuts FROM participacions WHERE id_jugador = v_jugador.id_jugador;
  v_jugador.prima := v_minuts*1000 + v_jugador.prima;
  SELECT COUNT(*) INTO v_guanyat FROM partits WHERE gols_local > gols_visitant AND UPPER(id_equip_local) = UPPER(v_jugador.id_equip);
  SELECT v_guanyat + COUNT(*) INTO v_guanyat FROM partits WHERE gols_local < gols_visitant AND UPPER(id_equip_visitant) = UPPER(v_jugador.id_equip);
    v_jugador.prima := v_guanyat*2000 + v_jugador.prima;
  RETURN v_jugador.prima;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN -1;
  WHEN DUP_VAL_ON_INDEX THEN
    RETURN -3;
  WHEN OTHERS THEN
    RETURN -3;
END FUN_PRIMA;

-- exercici 3

create or replace PROCEDURE GRAVA_PRIMA
(
  v_nom_jugador jugadors.nom_jugador%type
) AS
  v_prima_final jugadors.prima%type;
  v_pais equips.id_pais%type;
  v_conv_pais equips.id_pais%type;
BEGIN
  v_prima_final:= FUN_PRIMA(v_nom_jugador);
  SELECT id_pais INTO v_pais FROM equips WHERE id_equip = (SELECT id_equip FROM jugadors WHERE UPPER(nom_jugador) LIKE UPPER(v_nom_jugador));
  SELECT nom_pais INTO v_conv_pais FROM paisos WHERE UPPER(id_pais) = UPPER(v_pais);
  SELECT nom_pais INTO v_conv_pais FROM paisos WHERE UPPER(id_pais) = UPPER(v_pais);
  CASE v_conv_pais
    WHEN 'ESPANYA' THEN v_prima_final := v_prima_final * 2;
    WHEN 'ANGLATERRA' THEN v_prima_final := v_prima_final * 3;
  END CASE;
  UPDATE jugadors SET prima = v_prima_final WHERE UPPER(nom_jugador) LIKE UPPER(v_nom_jugador);
  dbms_output.put_line('S''ha modificat la prima: '||v_prima_final||' €');
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('El nom del jugador no existeix');
  WHEN DUP_VAL_ON_INDEX THEN
    dbms_output.put_line('Més d''un jugador amb aquest nom');
  WHEN OTHERS THEN
    dbms_output.put_line(SQLERRM||' - '||SQLCODE);
END GRAVA_PRIMA;

-- 1. Crea un procediment que es digui “generausers” que rebi el codi d'un empleat i mostri per pantalla aquest codi, el cognom, el seu nom d'usuari de la base de dades i el complement de “fidelitat” que el pagarem avui.
-- El nom d'usuari estarà format per les tres primeres lletres del seu cognom seguides del seu codi d'empleat.
-- El complement de “fidelitat” se calcula de la següent forma:
--  .- Si l'empleat és “venedor” 100 €, si és “empleat” 200 € i si és “analista” 300 €.
--  .- Per cada 500 dies que l'empleat porta treballant a l'empresa 50 € més. Les fraccions de 500 dies no es tenen en compte.
--  .- Si cobra menys que la mitjana dels salaris del seu departament 100 € més.
--  .- Els directors i el president no cobren aquest complement.
-- El procediment ha de contemplar tots els errors que es puguin produir i informar d'ells.
-- Especifica també els casos de prova i els resultats esperats!!

create or replace PROCEDURE generausers
(
  v_emp_no NUMBER
) AS
  v_emp emp%rowtype;
  v_fidelitat NUMBER;
  v_avg_salari NUMBER;
BEGIN
  SELECT * INTO v_emp FROM emp WHERE emp_no = v_emp_no;
  IF UPPER(v_emp.ofici) != 'DIRECTOR' OR UPPER(v_emp.ofici) != 'PRESIDENT' THEN
    IF UPPER(v_emp.ofici) = 'VENEDOR' THEN
      v_fidelitat := 100;
    ELSIF UPPER(v_emp.ofici) = 'EMPLEAT' THEN
      v_fidelitat := 200;
    ELSIF UPPER(v_emp.ofici) = 'ANALISTA' THEN
      v_fidelitat := 300;
    END IF;
    v_fidelitat := v_fidelitat + TRUNC((SYSDATE - v_emp.data_alta)/500)*50;
    SELECT AVG(salari) INTO v_avg_salari FROM emp WHERE dept_no = v_emp.dept_no;
    IF v_emp.salari < v_avg_salari THEN
      v_fidelitat := v_fidelitat + 100;
    END IF;
  ELSE
    v_fidelitat := 0;
  END IF;
  DBMS_OUTPUT.PUT_LINE(v_avg_salari || ' Codi Empleat: ' || v_emp.emp_no || ' Cognom: ' || v_emp.cognom || ' Nom Usuari: ' || CONCAT(SUBSTR(v_emp.cognom, 1, 3),v_emp_no) || ' Fidelitat: ' || NVL(v_fidelitat, 0) ||'€');
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('No s''ha trobat l''empleat');
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Codi error: '||SQLCODE||' - '||SQLERRM);
END generausers;

-- CASOS DE PROVA ---------------------------------------------------------
-- emp_no: 7499
-- · és vendedor + 100€
-- · dies a la empresa 14313 dies / 500 ==> 28,626 dies * 50€ => 28 * 50= 1400€
-- cobra més que la mitjana del seu departament ==> no suma més
-- · 1400€ + 100€ = fidelitat 1500 €
-- Codi Empleat: 7499 Cognom: ARROYO Nom Usuari: ARR7499 Fidelitat: 1500€

-- emp_no: 7900
-- és empleat ==> + 200
-- cobra menys mitjana del seu departament  ==> +100
-- Codi Empleat: 7900 Cognom: JIMENO Nom Usuari: JIM7900 Fidelitat: 1650€

-- emp_no: 7566
-- · és director ha de sortir fidelitat zero.
-- Codi Empleat: 7566 Cognom: JIMÉNEZ Nom Usuari: JIM7566 Fidelitat: 0€

-- emp_no: 9999
-- · No existeix ha de saltar l'exepció.
-- FI CASOS DE PROVA -------------------------------------------------------

-- 2. Crea una funció que rebi com a arguments el codi d'un client i un tipus de comanda (A, B, C o nul) i calculi i retorni l'import total de totes les comandes d'aquest tipus que ha fet el client.
-- En cas de què es demani un codi de client que no existeix la funció ha de tornar -1,
-- si es demana un tipus de comanda que no existeix la funció ha de tornar valor -2, si el client no ha fet cap comanda del tipus demanat la funció ha de tornar un 0.

create or replace FUNCTION TOTAL_COMANDES_CLIENT
(
  v_client_cod NUMBER,
  v_tipus_comanda comanda.com_tipus%type
) RETURN NUMBER AS
  v_total_client NUMBER;
  v_num_clients NUMBER;
BEGIN
  SELECT SUM(total) INTO v_total_client FROM comanda WHERE client_cod = v_client_cod AND com_tipus LIKE UPPER(v_tipus_comanda);
  SELECT COUNT(total) INTO v_num_clients FROM comanda WHERE client_cod = v_client_cod;

  IF v_num_clients = 0 THEN
    v_total_client := -1;
  ELSIF UPPER(v_tipus_comanda) NOT IN ('A','B','C') THEN
    v_total_client := -2;
  ELSE
    v_total_client := 0;
  END IF;

RETURN v_total_client;
END TOTAL_COMANDES_CLIENT;

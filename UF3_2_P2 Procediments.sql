-- 1. Fes un procediment que rebi com a paràmetre un número d'empleat i mostri per pantalla el seu cognom i ofici.
-- La taula per extreure les dades serà la "EMP". S'han de tractar els errors : "no troba dades", "la consulta retorna més d'una fila" i qualsevol altre.
create or replace PROCEDURE EX1
(
  v_numEmple emp.emp_no%type
)
AS
  v_cognom emp.cognom%type;
  v_ofici emp.ofici%type;
BEGIN
  SELECT cognom, ofici INTO v_cognom, v_ofici FROM emp WHERE emp_no = v_numEmple;
  dbms_output.put_line('Cognom: '||v_cognom||'Ofici: '||v_ofici);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No s''ha trobat l''empleat');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Més d''una filera');
  WHEN OTHERS THEN
    dbms_output.put_line('Codi error: '||SQLCODE||' - '||SQLERRM);
END EX1;

-- 2. Fes un procediment que rebi com a paràmetre el número d'un departament de la taula DEPT i mostri per pantalla el seu nom i localitat.
-- Has de comprovar els mateixos errors que a l'exercici 1.

CREATE OR REPLACE PROCEDURE EX2
(
  V_dept_no dept.dept_no%type
) AS
  v_dnom dept.dnom%type;
  v_loc dept.loc%type;
BEGIN
  SELECT dnom, loc INTO v_dnom, v_loc FROM dept WHERE dept_no = v_dept_no;
  dbms_output.put_line('Nom departament: '||v_dnom||'Nom localitat: '||v_loc);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No s''ha trobat el departament');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Més d''una filera');
  WHEN OTHERS THEN
    dbms_output.put_line('Codi error: '||SQLCODE||' - '||SQLERRM);
END EX2;

-- 3. Fes un procediment que rebi com a paràmetres el número d’empleat de la taula EMPLE i un nou sou de l’empleat. El procediment comprovarà que el número d’empleat existeix,
-- i, comprovarà també si el nou sou no supera en un 20% a l’antic. Si el codi de l’empleat es correcte i el nou sou no supera el 20% a l’antic,
-- llavors, modificarà la BD amb el nou sou; però, en cas contrari, avisarà del motiu de l’error.

CREATE OR REPLACE PROCEDURE EX3
(
  v_emp_no emp.emp_no%type
 ,v_nou_salari emp.salari%type
) AS
  v_salari emp.salari%type;
BEGIN
  SELECT salari INTO v_salari FROM emp WHERE emp_no = v_emp_no;
  IF v_nou_salari <= v_salari * 1.2 THEN
    UPDATE emp SET salari = v_nou_salari WHERE emp_no = v_emp_no;
  ELSE
  dbms_output.put_line('El salari supera en un 20% l''antic');
  END IF;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No s''ha trobat l''empleat');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Més d''una filera');
  WHEN OTHERS THEN
    dbms_output.put_line('Codi error: '||SQLCODE||' - '||SQLERRM);
END EX3;

-- 4. Crea un procediment que doni d'alta 10 nous empleats en la taula "emple". El número d'empleat del primer serà el 8000 i el de l'últim el 8009.
-- Tots es diran "dasi" de cognom, seran "empleados"', tindran de director a l'empleat número 7839, data d'alta la d'avui, guanyaran tots 1500 euros,
-- tindran tots una comissió de 100 euros i seran del departament 20. Ha de mostrar per pantalla un missatge amb el número d'empleats que tenia abans el departament 20 i el número d'empleats que té ara.

CREATE OR REPLACE PROCEDURE EX4_NOUS_EMPLEATS AS
  v_empT NUMBER;
BEGIN
  SELECT count(emp_no) INTO v_empT FROM emp;
  DBMS_OUTPUT.put_line('Numero d''empleats inicials: '||v_empT);
  FOR i IN 8000 .. 8009 LOOP
    INSERT INTO emp VALUES(i,'Dasi','EMPLEAT',7839,SYSDATE,1500,100,20);
  END LOOP;
  SELECT count(emp_no) INTO v_empT FROM emp;
  DBMS_OUTPUT.put_line('Numero d''empleats finals: '||v_empT);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No s''ha trobat l''empleat');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Més d''una filera');
  WHEN DUP_VAL_ON_INDEX THEN
    dbms_output.put_line('Clau primaria duplicada');
  WHEN OTHERS THEN
    dbms_output.put_line('Codi error: '||SQLCODE||' - '||SQLERRM);
END EX4_NOUS_EMPLEATS;

-- 5. Crea un procediment que esborri els empleats que has creat a l'exercici anterior i mostri per pantalla quants empleats has eliminat.

CREATE OR REPLACE PROCEDURE EX5_ELIMINAR_EMPLEATS AS
  v_empT NUMBER;
BEGIN
  SELECT count(emp_no) INTO v_empT FROM emp;
  dbms_output.put_line('Numero d''empleats inicials: '||v_empT);
  DELETE FROM emp WHERE emp_no BETWEEN 8000 AND 8009;
  SELECT count(emp_no) INTO v_empT FROM emp;
  dbms_output.put_line('Numero d''empleats finals: '||v_empT);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No s''ha trobat l''empleat');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Més d''una filera');
  WHEN OTHERS THEN
    dbms_output.put_line('Codi error: '||SQLCODE||' - '||SQLERRM);
END EX5_ELIMINAR_EMPLEATS;

-- 6. Crea un procediment que doni d'alta 5 nous empleats en la taula "emple". El número d'empleat del primer serà el 9000 i el de l'últim el 9004.
-- Es diran "dasi1", "dasi2", "dasi3" etc de cognom, tindran d'ofici "vendedor" , de director al Sr. Fernandez , data d'alta una setmana a partir d'avui,
-- guanyaran tots 1500, 1510, 1520 etc euros, tindran tots una comissió de 100 euros i seran del departament 30.
-- Ha de mostrar un missatge amb el número d'empleats que tenia al seu càrrec el Sr. Fernandez abans d'executar el procediment i després d'executar-lo.

CREATE OR REPLACE PROCEDURE EX6_CREAR_5_EMPLEATS AS
  v_ContCognom NUMBER := 1;
  v_ContSalari NUMBER := 1500;
  v_ContFer NUMBER;
  v_cap NUMBER;
BEGIN
  SELECT emp_no INTO v_cap FROM emp WHERE UPPER(cognom) LIKE 'FERN_NDEZ';
  SELECT count(emp_no) INTO v_ContFer FROM emp WHERE CAP LIKE v_cap;
  dbms_output.put_line('Empleats inicials al càrrec de Fernandez: '||v_ContFer);
  FOR i IN 9000 .. 9004 LOOP
    INSERT INTO emp VALUES(i,CONCAT('dasi',v_ContCognom),'VENEDOR',v_cap,SYSDATE + 7,v_ContSalari,100,30);
    v_ContCognom := v_ContCognom + 1;
    v_ContSalari := v_ContSalari + 10;
  END LOOP;

-- Versió 2 (una variable menys)
  -- FOR i IN 0 .. 4 LOOP
  --   INSERT INTO emp VALUES(i+9000,CONCAT('dasi',i),'VENEDOR',v_cap,SYSDATE + 7,v_ContSalari,100,30);
  --   v_ContSalari := v_ContSalari + 10;
  -- END LOOP;
  --
  SELECT count(emp_no) INTO v_ContFer FROM emp WHERE CAP LIKE v_cap;
  dbms_output.put_line('Empleats finals al càrrec de Fernandez: '||v_ContFer);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No s''han trobat trobat el o els empleats');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Més d''una filera');
  WHEN DUP_VAL_ON_INDEX THEN
    dbms_output.put_line('Clau primaria duplicada');
  WHEN OTHERS THEN
    dbms_output.put_line('Codi error: '||SQLCODE||' - '||SQLERRM);
END EX6_CREAR_5_EMPLEATS;

-- 7. Crea un procediment que esborri els empleats que has creat a l'exercici anterior.
-- Ha de mostrar un missatge amb el número d'empleats que tenia al seu càrrec el Sr.
-- Fernandez abans d'executar el procediment i després d'executar-lo.

CREATE OR REPLACE PROCEDURE EX6_ELIMINAR_5_EMPLEATS AS
  v_ContFer NUMBER;
  v_cap NUMBER;
BEGIN
  SELECT emp_no INTO v_cap FROM emp WHERE UPPER(cognom) LIKE 'FERN_NDEZ';
  SELECT count(emp_no) INTO v_ContFer FROM emp WHERE CAP LIKE v_cap;
  dbms_output.put_line('Empleats inicials al càrrec de Fernandez: '||v_ContFer);
  DELETE FROM emp WHERE emp_no BETWEEN 9000 AND 9004;
  SELECT count(emp_no) INTO v_ContFer FROM emp WHERE CAP LIKE v_cap;
  dbms_output.put_line('Empleats finals al càrrec de Fernandez: '||v_ContFer);
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Codi error: '||SQLCODE||' - '||SQLERRM);
END EX6_ELIMINAR_5_EMPLEATS;

-- 1.	Es necessita portar un històric dels canvis realitzats en les condicions
-- contractuals dels empleats que estan enregistrades a la taula emple. Per això
-- primer has de crear una taula de nom “auditaemple” amb tres camps, un de nom
-- “id_canvi” number(5) que serà la clau primària, altre de nom “descripcio_canvi”
-- de tipus varchar2(100) i un altre de nom “data_canvi” de tipus date.

CREATE TABLE AUDITAEMPLE
(
  ID_CANVI NUMBER(5)
, DESCRIPCIO_CANVI VARCHAR2(100)
, DATA_CANVI DATE NOT NULL
, CONSTRAINT AUDITAEMPLE_PK PRIMARY KEY
  (
    ID_CANVI
  )
  ENABLE
);

-- Crea un trigger que es digui “auditasou” i serveixi per què cada vegada que es
-- canviï el salari d'un empleat s'inserixi a la taula “auditaemple” un registre
-- amb el missatge “El salari de l'empleat codiempleat abans era de souantic i ara
-- serà sounou” , la data actual i de id_canvi el mès alt que hi haguès a la taula
-- incrementat en 1.

CREATE OR REPLACE TRIGGER AUDITASOU
  AFTER UPDATE OF SALARI ON EMP
  FOR EACH ROW WHEN (new.salari <> old.salari)
DECLARE
  v_id_canvi AUDITAEMPLE.ID_CANVI%TYPE := 0;
BEGIN
  SELECT NVL(MAX(id_canvi),0) INTO v_id_canvi FROM AUDITAEMPLE;
  INSERT INTO auditaemple VALUES(v_id_canvi + 1,CONCAT('El salari de l''empleat ',
    CONCAT(:old.emp_no, CONCAT(' abans era de ', CONCAT(:old.salari, CONCAT(' i ara serà ', :new.salari))))),SYSDATE);
END;

-- 2.	Fes un altre trigger de nom “auditaemple” que sigui una modificació de
-- l'anterior per afegir que si es dona d'alta un nou empleat s'enregistri a la
-- taula “auditaemple” el missatge “Nou empleat amb codi emp_no” amb data d'avui.

create or replace TRIGGER AUDITAEMPLE
  AFTER UPDATE OF SALARI OR INSERT ON EMP
  FOR EACH ROW
DECLARE
  v_id_canvi AUDITAEMPLE.ID_CANVI%TYPE := 0;
BEGIN
  SELECT NVL(MAX(id_canvi),0) INTO v_id_canvi FROM AUDITAEMPLE;
  IF updating AND :new.salari <> :old.salari THEN
  INSERT INTO auditaemple VALUES(v_id_canvi + 1, CONCAT('El salari de l''empleat ',
    CONCAT(:old.emp_no, CONCAT(' abans era de ', CONCAT(:old.salari, CONCAT(' i ara serà ', :new.salari))))),SYSDATE);
  END IF;
  IF inserting THEN
    INSERT INTO auditaemple VALUES(v_id_canvi + 1, CONCAT('Nou empleat amb codi ', :old.emp_no),SYSDATE);
  END IF;
END;

-- 3.	Fes un altre trigger de nom “auditaemple2” que sigui una modificació de
-- l'anterior i només es dispari en el cas de què s'intenti canviar un salari si la
-- modificació consisteix en pujar el salari a l'empleat més d'un 10%.

create or replace TRIGGER auditaemple2
  AFTER UPDATE OF SALARI OR INSERT ON EMP
  FOR EACH ROW WHEN (new.salari > old.salari * 1.1)
DECLARE
  v_id_canvi AUDITAEMPLE.ID_CANVI%TYPE := 0;
BEGIN
  SELECT NVL(MAX(id_canvi),0) INTO v_id_canvi FROM AUDITAEMPLE;
  IF updating AND :new.salari <> :old.salari THEN
  INSERT INTO auditaemple VALUES(v_id_canvi + 1, CONCAT('El salari de l''empleat ',
    CONCAT(:old.emp_no, CONCAT(' abans era de ', CONCAT(:old.salari, CONCAT(' i ara serà ', :new.salari))))),SYSDATE);
  END IF;
END;

-- 4.	Crea un trigger de nom “verificaunitats” que serveixi perquè en cap línia de
-- detall es puguin demanar més de 999 unitats. La forma de què un trigger pugui
-- evitar que es porti a terme l'operació de dispar és aixecar una excepció i no
-- tractar-la. Fa falta un trigger per fer això?
-- No fa falta, podem limitarlo mitjançant la longitut

CREATE OR REPLACE TRIGGER VERIFICAUNITATS
  BEFORE UPDATE OF QUANTITAT OR INSERT ON DETALL
  FOR EACH ROW WHEN (new.quantitat > 999)
  DECLARE quantitat_maxima_superada EXCEPTION;
BEGIN
  RAISE quantitat_maxima_superada;
END;

-- 5.	Modifica el trigger de l'exercici anterior. S'ha de dir “verificaunitats2” i
-- ha de fer el següent: Si s'intenta posar una quantitat superior a 999 en
-- qualsevol línia de detall s'enregistrarà quantitat = 1000.

CREATE OR REPLACE TRIGGER VERIFICAUNITATS2
  BEFORE UPDATE OF QUANTITAT OR INSERT ON DETALL
  FOR EACH ROW WHEN (new.quantitat > 999)
BEGIN
  :new.quantitat := 1000;
END;

-- 5. Llegueix l'exemple de disparadors que figura al final de la pàgina 93 dels
-- apunts de l'IOC. Fent servir l'assistent que té el Developer per crear triggers
-- crea el trigger “detall_before” que hi ha a la pàgina 94 del apunts de l'IOC.
-- Observa que en realitat el “if” no es necessita per res.  Prova el funcionament
-- d'aquest trigger modificant els camps quantitat, preu o total d'alguna línia de
-- detall o inserint una de nova (Fes-ho abans de llegir als apunts com cal
-- fer-ho). * No cal que escriguis els updates o inserts “a mà”. Pots fer servir el
-- developer per modificar les dades directament sobre la graella de visualització
-- de la taula.

CREATE OR REPLACE TRIGGER DETALL_BEFORE
  BEFORE INSERT OR UPDATE OF PREU_VENDA,QUANTITAT,IMPORT
  ON DETALL
  FOR EACH ROW
BEGIN
-- Si fa un instert o un update el nou import es calcula multiplicant la nova quantitat amb el nou preu_venda
  IF inserting or updating then
    :new.import := :new.quantitat * :new.preu_venda;
  END IF;
END DETALL_BEFORE;

-- +------------+-----------+--------+
-- | PREU_VENDA | QUANTITAT | IMPORT |
-- +------------+-----------+--------+
-- |     40     |     1     |   40   |
-- +------------+-----------+--------+
-- S'hi cambio la quantitat, el preu de venda o l'import tornara a calcular l'import total (preu_venda * quantitat)
-- Si inserim una nova filera, recalcula l'import total (Hem d'introduir la comanda prèviament).

-- 6. Crea el trigger “detall_after” que hi ha a la pàgina 94 dels
-- apunts. Assegura't de què entens què es fa a cada “if” afegint comentaris que
-- expliquin què es fa en cadascun. Prova el funcionament d'aquest trigger
-- modificant els camps quantitat, preu o total d'alguna línia de detall, inserint
-- una de nova i canviant de número de comanda a alguna línia. (Fes-ho abans de
-- llegir als apunts com cal fer-ho). Estan ara correctes tots els totals de les
-- comandes? (El total d'una comanda ha de ser la suma dels imports de totes les
-- seves línies)

CREATE OR REPLACE TRIGGER DETALL_AFTER
  AFTER DELETE OR INSERT OR UPDATE OF COM_NUM,PREU_VENDA,QUANTITAT
  ON DETALL
  FOR EACH ROW
BEGIN
-- Si fa un delete o un update amb el número de comanda diferent, al total l'hi resta l'import anterior.
  IF deleting OR (updating AND :new.com_num<>:old.com_num) THEN
    UPDATE comanda SET total = total - :old.import WHERE com_num = :new.com_num;
  END IF;
-- Si fa un insert o un update amb el número de comanda diferent, l'hi suma el nou import al total.
  IF inserting OR (updating AND :new.com_num<>:old.com_num) THEN
    UPDATE comanda SET total = total + :new.import WHERE com_num = :new.com_num;
  END IF;
-- Si fa un update el nou número de comanda és igual a l'anterior, al total l'hi suma el nou import i li resta l'antic.
  IF updating AND :new.com_num = :old.com_num THEN
    UPDATE comanda SET total = total + :new.import - :old.import WHERE com_num = :new.com_num;
  END IF;
END DETALL_AFTER;

-- +----------+------------+-----------+
-- | COM_NUM  | PREU_VENDA | QUANTITAT |
-- +----------+------------+-----------+
-- |   610    |      40    |    1      |
-- +----------+------------+-----------+
-- Quan canvien el preu de veda o la quantitat el trigger canvia el total de la comanda després que "detall_before" actualitzi l'import
-- El numero de comanda no es pot modificar ORA-02291: restricción de integridad (EMPRESAIOC.DETALL_FK_COMANDA) violada - clave principal no encontrada

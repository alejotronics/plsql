-- 1. Crea un procediment que es digui “calculaimport” que rebi el nom i cognom d'un nen, el codi d'una casa i una data d'entrada
-- en la casa i enregistri a l'atribut “import” de la taula “nens_cases” l'import de l'estada d'aquell nen en aquella casa.
-- .- Si hi ha més d'un nen amb aquests mateixos nom i cognom s'haurà de mostrar un missatge “Nom i cognom repetits, s'ha d'accedir pel codi del nen”.
-- .- Si no hi ha cap nen amb aquest nom i cognom, no existeix cap casa amb aquest codi o no hi ha cap estada d'aquest nen en aquesta casa en la data d'entrada proporcionada s'ha de mostrar un missatge informatiu d'aquestes circumstàncies (es pot mostrar un únic missatge per a totes aquestes situacions però es valorarà positivament que es mostri un missatge diferent per a cadascuna).
-- .- Si hi ha una estada del nen demanat a la casa demanada i en la data d'entrada demanada però l'estada encara no ha acabat (el nen encara hi és a la casa) s'ha de mostrar un missatge informant de què no es pot calcular l'import fins que el nen no marxi.
-- .- Si sí que hi ha una estada del nen demanat, a la casa demanada, amb la data d'entrada demanada i el nen ja ha marxat l'import es calcula tenint en compte el nombre de dies que ha passat a la casa i el preu diari de la casa. Després de registrar aquest import s'ha de mostrar un missatge informant de què s'ha gravat i de l'import enregistrat.
-- El procediment ha de contemplar també qualsevol altre error que es pugui produir i informar d'ell.

create or replace PROCEDURE calculaimport
(
  v_nom VARCHAR2,
  v_cognom VARCHAR2,
  v_cod_casa cases.codcasa%type,
  v_data_entrada DATE
) AS
  v_import nens_cases.importe%type;
  v_dies_estada NUMBER;
  v_datasortida DATE;
  v_codnen nens_cases.codnen%type;
BEGIN
  SELECT codnen INTO v_codnen FROM nens WHERE UPPER(nom) LIKE UPPER(v_nom) AND UPPER(cognom) LIKE UPPER(v_cognom);
  SELECT datasortida INTO v_datasortida FROM nens_cases WHERE codnen = v_codnen AND dataentrada LIKE v_data_entrada;
  IF v_datasortida IS NULL THEN
    dbms_output.put_line('El nen, encara es troba a la casa');
  ELSE
    SELECT datasortida - dataentrada INTO v_dies_estada FROM nens_cases WHERE codnen = v_codnen AND dataentrada LIKE v_data_entrada;
    SELECT preudia * v_dies_estada INTO v_import FROM cases WHERE codcasa = v_cod_casa;
    UPDATE nens_cases SET importe = v_import WHERE codnen = v_codnen;
    dbms_output.put_line('S''ha registrar l''import del nen amb codi '||v_codnen||'. Import total: '||v_import||' €');
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('El nen, la casa o el dia d''entrada no s''han trobat');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Nom i cognoms repetit, s''ha d''accedir pel codi del nen');
  WHEN OTHERS THEN
    dbms_output.put_line(SQLERRM||' - '||SQLCODE);
END calculaimport;

-- nen unic amb datasortida
-- codnen: 12 nom: Pau cognom: Castells codcasa: 4 dataentrada: 01/07/15
-- import: 403,7€

-- nen unic sese datasortida
-- codnen: 2 nom: Alex cognom: Scotti codcasa: 1 dataentrada: 22/04/19
-- missatge 'El nen, encara es troba a la casa'

-- 2. Crea una funció que es digui “places” que rebi com a argument el nom d'una comarca i retorni el total de places disponibles en les cases de colònies que hi ha en la comarca.
  -- Si la comarca demanada no es troba a la nostra base de dades la funció ha de tornar un -1.
  -- Si en la comarca demanada no hi ha cap casa de colònies la funció ha de tornar un 0.
  -- Si es produeix qualsevol altre error la funció ha de tornar un -2.

  CREATE OR REPLACE FUNCTION PLACES_LLIURES
  (
    v_NomCom comarques.nom%type
  ) RETURN NUMBER AS
    v_codcom comarques.codcom%type;
    v_places_lliures NUMBER;
    v_capacitat NUMBER;
  BEGIN
    SELECT codcom INTO v_codcom FROM comarques WHERE UPPER(nom) LIKE UPPER(v_NomCom);
    SELECT SUM(capacitat) INTO v_capacitat FROM cases WHERE codcom = v_codcom;

    IF v_capacitat IS NULL THEN
      RETURN 0;
    ELSE
    SELECT COUNT(*) INTO v_places_lliures FROM nens_cases
      WHERE codcasa IN (SELECT codcasa FROM cases WHERE codcom = v_codcom) AND datasortida IS NULL;
      v_places_lliures := v_capacitat - v_places_lliures;
    RETURN v_places_lliures;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN -1;
    WHEN OTHERS THEN
      RETURN -2;
  END PLACES_LLIURES;

-- 3. Crea una funció que es digui “densitat” que rebi com a argument el nom d'una casa de colònies
--    i retorni la densitat de població de la comarca en què es troba la casa amb una precisió de dues xifres decimals.
-- Si la casa demanada no es troba a la nostra base de dades la funció ha de tornar un 0.
-- Si es produeix qualsevol altre error la funció ha de tornar un -1.

CREATE OR REPLACE FUNCTION DENSITAT
(
  v_NomCasa cases.nom%type
) RETURN NUMBER AS
  v_codcom cases.codcom%type;
  v_densitat NUMBER(10,2);
BEGIN
  SELECT codcom INTO v_codcom FROM cases WHERE UPPER(nom) LIKE UPPER(v_NomCasa);
  SELECT TRUNC(nombrehab/(SELECT superficie FROM comarques WHERE codcom = v_codcom),2) INTO v_densitat FROM comarques WHERE codcom = v_codcom;
  RETURN v_densitat;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
  WHEN OTHERS THEN
    RETURN -1;
END DENSITAT;

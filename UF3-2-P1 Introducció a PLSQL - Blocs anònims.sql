-- 1. Bloc anònim que demani un codi d’empleat i mostri les seves dades.

ACCEPT v_emp PROMPT "Introdueix el codi del empleat"
DECLARE
  v_emp emp.emp_no%type;
  v_cognom emp.cognom%type;
  v_ofici emp.ofici%type;
  v_cap emp.cap%type;
  v_dataAlta emp.data_alta%type;
  v_salari emp.salari%type;
  v_comissio emp.comissio%type;
  v_deptNo emp.dept_no%type;
BEGIN
  SELECT cognom, ofici, cap, data_alta, salari, comissio, dept_no INTO v_cognom, v_ofici, v_cap, v_dataAlta, v_salari, v_comissio, v_deptNo FROM emp WHERE emp_no=&v_emp;
  DBMS_OUTPUT.put_line('Codi Empleat: ' || v_emp || ' Cognom: ' || v_cognom || ' Ofici: ' || v_ofici || ' Cap: ' || v_cap || ' Data Alta: '|| v_dataAlta || ' Salari: ' || v_salari || ' Comissió: ' || v_comissio);
END;

--V.2
ACCEPT v_emp PROMPT "Introdueix el codi del empleat"
DECLARE
  v_emple emp%ROWTYPE;
BEGIN
  SELECT * INTO v_emple FROM emp WHERE emp_no = &v_emp;
  DBMS_OUTPUT.put_line('Codi Empleat: ' || v_emple.emp_no || ' Cognom: ' || v_emple.cognom || ' Ofici: ' || v_emple.cap || ' Cap: ' || v_emple.cap || ' Data Alta: '|| v_emple.Data_ALTA || ' Salari: ' || v_emple.salari || ' Comissió: ' || v_emple.comissio || 'Departamient: ' || v_emple.dept_no);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No he trobat informació');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Massa files');
  WHEN OTHERS THEN
    dbms_output.put_line('Missatge error: '||SQLERRM||' Codi error: '||SQLCODE);
END;
-- 2. Bloc anònim que mostri el codi i nom del departament que tingui el codi més gran.

DECLARE
  codi dept.dept_no%type;
  nom dept.dnom%type;
BEGIN
  SELECT MAX(dept_no) INTO codi FROM dept;
  SELECT dnom INTO nom FROM dept WHERE dept_no = codi;
  dbms_output.put_line('Numero de departament '||codi||' Nom del departament '||nom);
END;

-- 3. Bloc anònim que afegeixi un nou departament on el seu codi serà el valor del codi més alt actualment existent més 10. El nom del departament es demanarà per teclat.

ACCEPT deptNom PROMPT 'Introdueix el nom del departament:';
DECLARE
  deptNom dept.dnom%type;
  codi dept.dept_no%type;
BEGIN
  SELECT MAX(dept_no) INTO codi FROM dept;
  codi := codi + 10;
  INSERT INTO dept VALUES (codi,'&deptNom',NULL);
  dbms_output.put_line('Numero departament '||codi||' Nom departament '||'&deptNom');
END;

-- 4. Fes un bloc anònim que llegeixi el codi del departament que has donat d'alta a l'exercici anterior i la localitat, i modifiqui el departament amb la nova localitat.

ACCEPT local PROMPT 'Introdueix el nom localitat:';
DECLARE
  local dept.loc%type;
  codi dept.dept_no%type;
BEGIN
  SELECT MAX(dept_no) INTO codi FROM dept;
  UPDATE dept SET loc = '&local' WHERE dept_no = &codi;
  dbms_output.put_line('Numero departament '||codi||' Localitat: '||'&local');
END;

-- 5. Bloc anònim que esborri els empleats que pertanyen al departament que llegim per teclat. El que introduïm és el nom del departament. EL programa mostrarà un missatge amb el número d’empleats esborrats. (Per saber el número d'empleats eliminats necessitaràs fer servir “SQL%rowcount”, mira el seu significat al document de teoria).

ACCEPT nomDept PROMPT 'Introdueix el nom departament:';
DECLARE
  nomDept dept.dnom%type;
  numDept dept.dept_no%type;
  filas NUMBER(2,0);
BEGIN
  SELECT dept_no INTO numDept FROM dept WHERE dnom = &nomDept;
  DELETE FROM emp WHERE dept_no = numDept;
  filas := SQL%ROWCOUNT;
  dbms_output.put_line('Numero de files modificades: '||filas);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No he trobat informació');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Massa files');
  WHEN OTHERS THEN
    dbms_output.put_line('Missatge error: '||SQLERRM||' Codi error: '||SQLCODE);
END;

-- 6. Bloc anònim que demani per pantalla el codi d'un empleat i l'esborri. Abans d’esborrar-lo cal que mostri les seves dades per pantalla (pots agafar exercici 1). Prova posar un codi d'empleat que no existeixi, un que sigui el cap d'algun altre i un que existeixi i no sigui el cap de ningú i observa els errors que retorna el bloc en cada cas.

ACCEPT v_emp PROMPT 'Introdueix el codi del empleat'
DECLARE
v_emple emp%ROWTYPE;
BEGIN
  SELECT * INTO v_emple FROM emp WHERE emp_no = &v_emp;
  DBMS_OUTPUT.put_line('Codi Empleat: ' || v_emple.emp_no || ' Cognom: ' || v_emple.cognom || ' Ofici: ' || v_emple.cap || ' Cap: ' || v_emple.cap || ' Data Alta: '|| v_emple.Data_ALTA || ' Salari: ' || v_emple.salari || ' Comissió: ' || v_emple.comissio || 'Departamient: ' || v_emple.dept_no);
  DELETE FROM emp WHERE emp_no = v_emple.emp_no;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No he trobat informació');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Massa files');
  WHEN OTHERS THEN
    dbms_output.put_line('Missatge error: '||SQLERRM||' Codi error: '||SQLCODE);
END;

-- 7. Mostrar tots els empleats que pertanyen al departament el codi del qual es demana per pantalla i que tenen com ofici el que s’entra per teclat.

-- 8. Eliminar tots els empleats que pertanyen al departament el codi del qual el demanem per pantalla i que tenen com ofici “Venedor”. Fes proves amb departaments que no existeixin, que no tinguin cap venedor i que sí tinguin venedors, i observa els errors que es retornen.

ACCEPT numDept PROMPT 'Introdueix el numero de departament:';
DECLARE
  numDept dept.dept_no%type;
BEGIN
  DELETE FROM emp WHERE dept_no = &numDept AND UPPER(ofici) LIKE 'VENEDOR';
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('No he trobat informació');
  WHEN TOO_MANY_ROWS THEN
    dbms_output.put_line('Massa files');
  WHEN OTHERS THEN
    dbms_output.put_line('Missatge error: '||SQLERRM||' Codi error: '||SQLCODE);
END;
